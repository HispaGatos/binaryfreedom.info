package main

import (
	"bufio"
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"regexp"
	"strings"
	"time"

	"github.com/mattn/go-mastodon"
	"gitlab.com/rek2/insert"
	"gitlab.com/rek2/removelines"
	"gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing/object"
)

var (
	content string
	path    string
	memtoot []string
)

const tootMsg string = " Your Toot has been selected for the next edition of the  binaryfreedom news https://binaryfreedom.info"

type Configuration struct {
	Email        string
	GitEmail     string
	Password     string
	Hugo	     string
	Path         string
	Server       string
	ClientID     string
	ClientSecret string
	GitDirectory string
}

func Contains(a []string, x string) bool {
	for _, n := range a {
		if x == n {
			return true
		}
	}
	return false
}

func msgAuthor(username string, c *mastodon.Client) {

	// check if toot was already sent to same author
	if Contains(memtoot, username) {
		return
	}

	// build toot
	msgtoot := "@" + username + tootMsg

	// Open toot for Author
	_, err := c.PostStatus(context.Background(), &mastodon.Toot{
		Status: msgtoot,
	})

	if err != nil {
		log.Fatal(err)

	}

	memtoot = append(memtoot, username)
}

func populatePost(postname, avatar, username string) {

	// construct the whole path with post name
	filepath := fmt.Sprintf("%s%s.md", path, postname)

	// remove from the post file line 2, one time
	err1 := removelines.Remove(filepath, 2, 1)
	if err1 != nil {
		log.Fatal(err1)
	}

	// Add string to line 2
	err2 := insert.InsertStringToFile(filepath, "image: "+avatar+"\n", 2)
	if err2 != nil {
		log.Fatal(err2)
	}

	// Add string to line 2
	err3 := insert.InsertStringToFile(filepath, "thumbnail: "+avatar+"\n", 2)
	if err3 != nil {
		log.Fatal(err3)
	}

	// Add string to line 1
	err4 := insert.InsertStringToFile(filepath, "title: "+username+" says...\n", 1)
	if err4 != nil {
		log.Fatal(err4)
	}

	// Open file write only appending the new data
	file, err := os.OpenFile(filepath, os.O_APPEND|os.O_WRONLY, 0660)
	if err != nil {
		log.Fatal(err)
	}

	// build a new bufio stream
	datawriter := bufio.NewWriter(file)

	// print to the bufio stream post content
	fmt.Fprint(datawriter, content)
	datawriter.Flush()
	file.Close()

}

func format(toot *mastodon.Status, gitTree *git.Worktree, c *mastodon.Client, hugopath string) {

	// set max tittle size
	tittleSize := 70

	// rule to remove empty spaces
	noSpace := regexp.MustCompile(" ")

	// here we save content into a string so we can post later
	content = string(toot.Content)

	// filter our own toots to other people
	regex := regexp.MustCompile("Your Toot has been selected for the next edition of the  binaryfreedom news")

	if regex.MatchString(content) {
		return
	}

	// Here we cut the toot to create the post tittle

	tittle := []rune(string(toot.Content))
	if len(tittle) >= tittleSize {
		cutTittleChars := string(tittle[0:60])

		if strings.Contains(cutTittleChars, "/tags/") {
			return
		}
		// replace/remove odd characters for title
		filter := strings.NewReplacer("/", "",
			"(", "",
			"https", "",
			"http", "",
			")", "",
			"?", "",
			"@", "",
			"!", "",
			"&", "",
			"$", "",
			"'", "",
			"<br>", "",
			"<p>", "",
			"\"", "",
			";", "",
			",", "",
			"<a href=", "",
			"class=", "",
			"<span", "",
			"</a", "",
			">", "",
			"*", "",
			":", "",
			"“", "",
			"<", "",
			"=", "",
			"|", "",
			".", "")

		cutTittle := filter.Replace(cutTittleChars)

		// We substitute spaces with underscores
		formatTittle := noSpace.ReplaceAllString(cutTittle, "_")

		// Format the argument with dinamic post name
		argument := fmt.Sprintf("posts/%s.md", formatTittle)

		// we run the command to create new post
		output, err := exec.Command(hugopath, "-v", "new", argument).Output()

		// check for error
		if err != nil {
			fmt.Println("Error on exec.Command Hugo")
			fmt.Println(argument)
			fmt.Println(string(output))
			log.Fatal(err)
		}

		avatar := toot.Account.Avatar
		username := toot.Account.Username
		display_name := toot.Account.DisplayName

		// limpiar DisplayName de :

		filter_dn := strings.NewReplacer(":", "")
		display_name = filter_dn.Replace(display_name)

		// Sent clean formated variables to create the post
		populatePost(formatTittle, avatar, display_name)

		// Sent info to author function
		msgAuthor(username, c)
		// add to git new post
		tempVariable := "content/english/" + argument
		fmt.Println(tempVariable)
		_, err = gitTree.Add(tempVariable)
		if err != nil {
			log.Fatal(err)
		}

	}
}

func main() {

	var config Configuration
	argPtr := flag.String("config", "config/config.production.json", "path to config file in json, check readme")

	flag.Parse()

	configFile, err := os.Open(*argPtr)
	defer configFile.Close()
	if err != nil {
		log.Fatal(err)
	}

	decoder := json.NewDecoder(configFile)
	err = decoder.Decode(&config)
	if err != nil {
		log.Fatal(err)
	}

	password := config.Password
	path = config.Path

	c := mastodon.NewClient(&mastodon.Config{
		Server:       config.Server,
		ClientID:     config.ClientID,
		ClientSecret: config.ClientSecret,
	})

	err1 := c.Authenticate(context.Background(), config.Email, password)
	if err1 != nil {
		log.Fatal(err1)
	}

	timeline, err := c.GetTimelineHome(context.Background(), nil)
	if err != nil {
		log.Fatal(err)
	}

	// open repo
	openGit, err := git.PlainOpen(config.GitDirectory)
	if err != nil {
		log.Fatal(err)
	}

	// open worktree
	gitTree, err := openGit.Worktree()
	if err != nil {
		log.Fatal(err)
	}

	gitTree.Status()

	// remove old posts from system and git

	removePostPath := "content/english/posts"
	if _, err := os.Stat(removePostPath); !os.IsNotExist(err) {
		_, mal := gitTree.Remove(removePostPath)
		if mal != nil {
			fmt.Println("Posts were not removed")
			log.Fatal(mal)
		}
	}

	for i := len(timeline) - 1; i >= 0; i-- {

		format(timeline[i], gitTree, c, config.Hugo)
	}

	// get time and update commit msg and commit
	afterTime := time.Now()
	commitMsg := "Updating site news at: " + afterTime.String()

	_, err = gitTree.Commit(commitMsg, &git.CommitOptions{
		Author: &object.Signature{
			Name:  "BinaryFredom Bot",
			Email: config.GitEmail,
			When:  time.Now(),
		},
	})

	// push all changes to gitlab so gitlab pages will format and publish
	err = openGit.Push(&git.PushOptions{})
	if err != nil {
		fmt.Println("Error on openGIT.Push")
		log.Fatal(err)
	}
}
