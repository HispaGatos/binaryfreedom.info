# binaryfreedom.info

New site for Binaryfreedom.info


![Alt text](/binaryfreedom-beta.png?raw=true "the new self made static generated with go bot binaryfreedom site")


# Things to take into account 
- make sure you create a special account just for this bot
- add your ssh priv key to ssh-add
- make sure ssh-agent is working/running
- CREATE *config folder* and the config file *config.production.json*
```
mkdir config
touch config/config.production.json
```

- example *config.production.json*
```
{
    "Email": "rek2@meow.cuddle",
    "GitEmail": "rek2wilds@hispagatos.meow",
    "Password": "xxxxxxxxxxx",
    "Hugo": "/usr/local/bin/hugo",
    "Path": "/home/rek2/binaryfreedom.info/content/english/posts/",
    "Server": "https://hispagatos.space",
    "ClientID": "80b3e9a6a19524xxXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxx",
    "ClientSecret": "ed4a2b069XxxXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXxxxx",
    "GitDirectory": "/home/rek2/binaryfreedom.info"
}
```


# Want to help?
- Visit https://gohugo.io/ for quickstart guide and full documentation.
