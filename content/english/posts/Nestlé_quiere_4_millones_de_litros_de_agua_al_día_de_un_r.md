---
title:  says...
date: 2019-08-27T12:00:04Z
thumbnail: https://hispagatos.space/avatars/original/missing.png
image: https://hispagatos.space/avatars/original/missing.png
draft: true
---

<p>Nestlé quiere 4 millones de litros de agua al día de un río protegido para venderlos</p><p>La compañía pretende comercializar agua de un paraje de Florida llamado Ginnie Springs, que se encuentra en pleno proceso de recuperación de caudal.</p><p><a href="https://www.lainformacion.com/management/nestle-quiere-4-millones-de-litros-de-agua-dia-rio-protegido-venderla/6511113/" rel="nofollow noopener" target="_blank"><span class="invisible">https://www.</span><span class="ellipsis">lainformacion.com/management/n</span><span class="invisible">estle-quiere-4-millones-de-litros-de-agua-dia-rio-protegido-venderla/6511113/</span></a></p>