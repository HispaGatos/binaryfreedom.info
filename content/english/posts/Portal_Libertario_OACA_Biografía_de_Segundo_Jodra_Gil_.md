---
title: Noticias anarquistas says...
date: 2019-08-27T12:00:02Z
thumbnail: https://hispagatos.space/system/accounts/avatars/000/080/152/original/57a430a901cbf2b3.png?1563397301
image: https://hispagatos.space/system/accounts/avatars/000/080/152/original/57a430a901cbf2b3.png?1563397301
draft: true
---

<p>Portal Libertario OACA: **Biografía de Segundo Jodra Gil (1907-1943)**</p><p>"Segundo Jodra Gil nació en 1907 en Pálmaces de Jadraque (Guadalajara). Carpintero, afiliado a la CNT, que en octubre de 1934 fue encarcelado brevemente en Puigcerdá junto a Antonio Martín, el que durante la Guerra civil se convirtió en destacado líder anarq…"</p><p><a href="http://www.portaloaca.com/historia/biografias/14442-biografia-de-segundo-jodra-gil-1907-1943.html" rel="nofollow noopener" target="_blank"><span class="invisible">http://www.</span><span class="ellipsis">portaloaca.com/historia/biogra</span><span class="invisible">fias/14442-biografia-de-segundo-jodra-gil-1907-1943.html</span></a></p><p><a href="https://newsbots.eu/tags/anarquismo" class="mention hashtag" rel="nofollow noopener" target="_blank">#<span>anarquismo</span></a> <a href="https://newsbots.eu/tags/bot" class="mention hashtag" rel="nofollow noopener" target="_blank">#<span>bot</span></a></p>