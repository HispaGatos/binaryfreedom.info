---
title: Purism says...
date: 2019-08-27T12:00:03Z
thumbnail: https://hispagatos.space/system/accounts/avatars/000/072/558/original/328a06b43c512b3f.png?1556727880
image: https://hispagatos.space/system/accounts/avatars/000/072/558/original/328a06b43c512b3f.png?1556727880
draft: true
---

<p>Did you know that with a <a href="https://social.librem.one/tags/free" class="mention hashtag" rel="nofollow noopener" target="_blank">#<span>free</span></a> Librem One account you get end-to-end encrypted chat, VoIP, and video calling used by millions of people <a href="https://librem.one" rel="nofollow noopener" target="_blank"><span class="invisible">https://</span><span class="">librem.one</span><span class="invisible"></span></a> <a href="https://social.librem.one/tags/libremone" class="mention hashtag" rel="nofollow noopener" target="_blank">#<span>libremone</span></a> <a href="https://social.librem.one/tags/privacy" class="mention hashtag" rel="nofollow noopener" target="_blank">#<span>privacy</span></a> <a href="https://social.librem.one/tags/security" class="mention hashtag" rel="nofollow noopener" target="_blank">#<span>security</span></a> <a href="https://social.librem.one/tags/voip" class="mention hashtag" rel="nofollow noopener" target="_blank">#<span>voip</span></a> <a href="https://social.librem.one/tags/slack" class="mention hashtag" rel="nofollow noopener" target="_blank">#<span>slack</span></a> <a href="https://social.librem.one/tags/skype" class="mention hashtag" rel="nofollow noopener" target="_blank">#<span>skype</span></a></p>