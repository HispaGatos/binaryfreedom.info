---
title: Purism says...
date: 2019-08-27T12:00:03Z
thumbnail: https://hispagatos.space/system/accounts/avatars/000/072/558/original/328a06b43c512b3f.png?1556727880
image: https://hispagatos.space/system/accounts/avatars/000/072/558/original/328a06b43c512b3f.png?1556727880
draft: true
---

<p>Purism Sale: Save 30%! Get $400 off and free shipping on UK and DE Librem 13 v4 laptops while stocks last <a href="https://shop.puri.sm/shop/librem-13/" rel="nofollow noopener" target="_blank"><span class="invisible">https://</span><span class="">shop.puri.sm/shop/librem-13/</span><span class="invisible"></span></a> <a href="https://social.librem.one/tags/purism" class="mention hashtag" rel="nofollow noopener" target="_blank">#<span>purism</span></a> <a href="https://social.librem.one/tags/lbrem13" class="mention hashtag" rel="nofollow noopener" target="_blank">#<span>lbrem13</span></a> <a href="https://social.librem.one/tags/linux" class="mention hashtag" rel="nofollow noopener" target="_blank">#<span>linux</span></a> <a href="https://social.librem.one/tags/linuxlaptop" class="mention hashtag" rel="nofollow noopener" target="_blank">#<span>linuxlaptop</span></a></p>