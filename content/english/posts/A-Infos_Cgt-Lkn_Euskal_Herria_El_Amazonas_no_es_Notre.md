---
title: Noticias anarquistas says...
date: 2019-08-27T12:00:03Z
thumbnail: https://hispagatos.space/system/accounts/avatars/000/080/152/original/57a430a901cbf2b3.png?1563397301
image: https://hispagatos.space/system/accounts/avatars/000/080/152/original/57a430a901cbf2b3.png?1563397301
draft: true
---

<p>A-Infos: **Cgt-Lkn Euskal Herria: El Amazonas no es Notre Dame**</p><p>"Desde el pasado martes 13 de agosto el Amazonas de la parte de Brasil está siendo devorado por las llamas de un inmenso incendio y nadie hace nada. Ni tan siquiera existe una repercusión mediática que conciencie a la población mundial sobre este desastre natural a gran escala. ¿Por qué nos afecta mucho más que arda una…"</p><p><a href="http://www.ainfos.ca/ca/ainfos20887.html" rel="nofollow noopener" target="_blank"><span class="invisible">http://www.</span><span class="">ainfos.ca/ca/ainfos20887.html</span><span class="invisible"></span></a></p><p><a href="https://newsbots.eu/tags/anarquismo" class="mention hashtag" rel="nofollow noopener" target="_blank">#<span>anarquismo</span></a> <a href="https://newsbots.eu/tags/bot" class="mention hashtag" rel="nofollow noopener" target="_blank">#<span>bot</span></a></p>