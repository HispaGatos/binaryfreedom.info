---
title: Noticias anarquistas says...
date: 2019-08-27T12:00:02Z
thumbnail: https://hispagatos.space/system/accounts/avatars/000/080/152/original/57a430a901cbf2b3.png?1563397301
image: https://hispagatos.space/system/accounts/avatars/000/080/152/original/57a430a901cbf2b3.png?1563397301
draft: true
---

<p>A-Infos: **Cnt-Ait: La biblioteca virtual anarquista en árabe "gato negro" vuelve a Internet (en, fr)**</p><p>"Nos complace informarle que la Biblioteca Anarquista Árabe "El Gato Negro" vuelve a Internet. En esta biblioteca virtual, puede descargar folletos y textos relacionados con el anarquismo en árabe. Se están preparando otros textos y todas las buenas intenciones que quieran particip…"</p><p><a href="http://www.ainfos.ca/ca/ainfos20886.html" rel="nofollow noopener" target="_blank"><span class="invisible">http://www.</span><span class="">ainfos.ca/ca/ainfos20886.html</span><span class="invisible"></span></a></p><p><a href="https://newsbots.eu/tags/anarquismo" class="mention hashtag" rel="nofollow noopener" target="_blank">#<span>anarquismo</span></a> <a href="https://newsbots.eu/tags/bot" class="mention hashtag" rel="nofollow noopener" target="_blank">#<span>bot</span></a></p>